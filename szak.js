/*
Copyright 2015 Kocsis Matyas <matyilona@openmaibox.org>

This file is part of MagicalOrarend.

MagicalOrarend is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

MagicalOrarend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MagicalOrarend. If not, see <http://www.gnu.org/licenses/>.
*/

//list of all szak-szakazon that the server returns ( only "%izik%" for now )
var szakAll = {};

function szak_callback( items ){
	for ( i in items ){
		$(items[i].elm).attr("id",items[i].values()["szakazon"]);
		}
	}

function make_szak_cell( szak, szakazon ){
	szakList.add( [{"szak":szak, "szakazon":szakazon}], szak_callback ); 
	}

function update_szakok( data, status ){
//get list of szakok
	if ( status != "success" ){
		console.log( "Szerverhiba szak frissitesnel " + status );
		return;
		}
	szak_lista = $("#szakokAll");
	var szakok = JSON.parse( data );
	for ( i in szakok ){
		make_szak_cell( szakok[ i ][ "szak" ], szakok[ i ][ "szakazon" ] );
		szakAll[ szakok[ i ][ "szak" ] ] = szakok[ i ][ "szakazon" ];
		}
	}

//list of szakazon s choosen
var szakChoosen = [];

function toggle_selection_szak( ) {
//toggle selection, delet form one list add to other
	var node = $(this);
	var szak_azon = node.attr("id");
	var selected = node.hasClass( "list-group-item-info" );
	var index = szakChoosen.indexOf( szak_azon );
	if ( selected ) {
		node.removeClass( "list-group-item-info" );
		szakChoosen.splice( index, 1 ); 
		}
	else {
		node.addClass( "list-group-item-info" );
		szakChoosen.push( szak_azon );
		}
	if ( szakChoosen.length != 0 ) {
		$.post( "test.php", { type : "kurzusok", szakChoosen : JSON.stringify( szakChoosen ) } , update_kurzusok );
		}
	else {
		clear_unselected_kurzusok();
		}
	}
