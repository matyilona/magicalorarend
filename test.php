<?php
/*
Copyright 2015 Kocsis Matyas <matyilona@openmaibox.org>

This file is part of MagicalOrarend.

MagicalOrarend is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

MagicalOrarend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MagicalOrarend. If not, see <http://www.gnu.org/licenses/>.
 */

//make PDO connection
include 'connect.php'; //connect to "real live" ttk server

function convert_row ( &$row ){
//convert database encoding, so json can eat it
		foreach( $row as $key => $val ){
			$row[ $key ] = mb_convert_encoding( $val, "UTF-8", "latin1" );
			}
}

// random AS for compatibility with js stuff


function list_szakok( ) {
//list szakok, in JSON
//TODO elozo felev vege alapjan valasszuk ki a mostanit -> ?mikor valt? ?valaszthato?
//TODO ejszakai/levelezo
	global $dbh;
	$szakok = array();
	foreach( $dbh->query("
		SELECT DISTINCT szak, szakparkod AS szakazon FROM
			szakok_szakparok INNER JOIN szakpar_kurzustipus
			WHERE tanev_felev='2016-2017-1';") as $row ) {
		convert_row( $row );
		$szakok[] =  $row;
		}
	echo json_encode( $szakok );
	}
	
function list_kurzusok( ) {
//list kurzusok in szakokChoosen
//TODO felevek itt is, ?tagozat?
	global $dbh;
	$szakok = json_decode( $_REQUEST[ "szakChoosen" ] );
	$sql = "
	SELECT DISTINCT kurzusok.kurzusnev AS kurzus, kurzusok.kurzuskod AS kurzkod, szakparplussz.szakparkod AS szakazon
		FROM kurzusok
		INNER JOIN beosztas ON kurzusok.kurzus_azonosito=beosztas.kurzus_azonosito
		INNER JOIN beosztas_szakpar ON beosztas_szakpar.beosztas_Azonosito=beosztas.beosztas_azonosito
		INNER JOIN szakparplussz ON szakparplussz.szakpar_az=beosztas_szakpar.szakpar_az
		INNER JOIN szakpar_kurzustipus ON szakpar_kurzustipus.szakpar_az=szakparplussz.szakpar_az
	WHERE szakparplussz.szakparkod IN (" . str_repeat( "?,", count( $szakok ) - 1 ) . "?)
		AND szakparplussz.tagozat='N'
		AND szakpar_kurzustipus.tanev_felev='2016-2017-1';";
	$stmt = $dbh->prepare( $sql );
	if ( $stmt->execute( $szakok ) ) {
		$kurzusok = array();
		while( $row = $stmt->fetch( ) ) {
			convert_row( $row );
			$kurzusok[] = $row;
			}
		echo json_encode( $kurzusok );
		}
	}

function list_kurzusok_bykurzkod( ) {
//list kurzusok by kurzkod
	global $dbh;
	$inkurzusok = json_decode( $_REQUEST[ "kurzusChoosen" ] );
	$sql = "SELECT DISTINCT kurzusok.kurzusnev AS kurzus, kurzusok.kurzuskod AS kurzkod FROM kurzusok
		WHERE kurzusok.kurzuskod IN (" . str_repeat( "?,", count( $inkurzusok ) - 1 ) . "?);";
	$stmt = $dbh->prepare( $sql );
	if ( $stmt->execute( $inkurzusok ) ) {
		$kurzusok = array();
		while( $row = $stmt->fetch( ) ) {
			convert_row( $row );
			$kurzusok[] = $row;
			}
		echo json_encode( $kurzusok );
		}
	}

function list_kurzus_data( ) {
//get list of kurzazon, list all kurzus data for timetable
//return list with each ora, with tanars already grouped
	global $dbh;
	$kurzusok = json_decode( $_REQUEST[ "kurzusChoosen" ] );
	//select kurzusok by kurzkod
	$sql_hely = "
		SELECT DISTINCT i.idopont AS kezd, ii.idopont AS vege, napok.nap AS nap,
		epuletek.epulet_neve AS epulet, termek.nev AS terem, 
		beosztas.csoport AS csopnum, beosztas.max_letszam, beosztas.megjegyzes, oratipus.oratipus,
		kurzusok.kurzuskod AS kurzkod, kurzusok.kurzusnev AS kurzus,
		kurzusok.oraszam_e AS elo, kurzusok.oraszam_g AS gyak, kurzusok.oraszam_l, kurzusok.kredit
		FROM beosztas_helye
		INNER JOIN idopontok i on i.idopontkod=beosztas_helye.orakezdes
		INNER JOIN idopontok ii on ii.idopontkod=beosztas_helye.oravege
		INNER JOIN termek ON termek.terem_azonosito = beosztas_helye.terem
		INNER JOIN epuletek ON termek.epuletkod = epuletek.epulet_kod
		INNER JOIN napok ON napok.napkod = beosztas_helye.nap
		INNER JOIN beosztas ON beosztas.beosztas_azonosito = beosztas_helye.beosztas_azonosito
		INNER JOIN oratipus ON oratipus.tipuskod = beosztas.oratipus
		INNER JOIN kurzusok ON kurzusok.kurzus_azonosito = beosztas.kurzus_azonosito
		WHERE kurzusok.kurzuskod IN (" . str_repeat( "?,", count( $kurzusok ) - 1 ) . "?);";
	//get tanarok for given csoport
	$sql_tanarok = "
		SELECT DISTINCT beosztas.csoport,
			oktatok.nev AS tanarnev, oktatok.email AS email,
			kurzusok.kurzuskod
		FROM beosztas
		INNER JOIN beosztott_oktatok ON beosztott_oktatok.beosztas_azonosito = beosztas.beosztas_azonosito
		INNER JOIN oktatok ON oktatok.tanar_azonosito = beosztott_oktatok.tanar_azonosito
		INNER JOIN kurzusok ON kurzusok.kurzus_azonosito = beosztas.kurzus_azonosito
		WHERE kurzusok.kurzuskod = ? AND beosztas.csoport = ?;";
	$stmt_hely = $dbh->prepare( $sql_hely );
	$stmt_tanarok = $dbh->prepare( $sql_tanarok );
	if ( $stmt_hely->execute( $kurzusok ) ) {
		$orak = [];
		while( $row = $stmt_hely->fetch( ) ) {
			convert_row( $row );
			$kurzkod = $row[ "kurzkod" ];
			$csopnum = $row[ "csopnum" ];
			$row[ "tanarok" ] = [];
			if ( $stmt_tanarok->execute( [ $kurzkod, $csopnum ] ) ){
				while( $tanar = $stmt_tanarok->fetch() ){
					convert_row( $tanar );
					$row[ "tanarok" ][] = [ "nev" => $tanar[ "tanarnev" ], "email" => $tanar[ "email" ] ];
					}
				}
			$orak[] = $row;
			}
		echo json_encode( $orak );
		}
	}
	


//let's see what we want
$type = $_REQUEST[ "type" ];
switch( $type ) {
	case "szakok":
		list_szakok( ); break;
	case "kurzusok":
		list_kurzusok( ); break;
	case "kurzusList":
		list_kurzusok_bykurzkod( ); break;
	case "orak":
		list_kurzus_data( ); break;
	default:
		echo "defaulted";
	}

//we're done close connection
$dhb = null;
?>
