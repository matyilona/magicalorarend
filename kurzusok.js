/*
Copyright 2015 Kocsis Matyas <matyilona@openmaibox.org>

This file is part of MagicalOrarend.

MagicalOrarend is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

MagicalOrarend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MagicalOrarend. If not, see <http://www.gnu.org/licenses/>.
*/

//list of kurzkod choosen
var kurzusChoosen = [];

function kurzus_callback( items ){
//sets up attributes for list element
	for ( i in items ){
		$(items[i].elm).attr( "id", items[i].values()["kurzkod"] );
		if (-1 != kurzusChoosen.indexOf( items[i].values()["kurzkod"] )){
			$(items[i].elm).addClass( "list-group-item-info" );
			}
		}
	}

function update_kurzusok( data, status ){
//get list of szakok
	clear_unselected_kurzusok();
	if ( status != "success" ){
		console.log( "Szerverhiba kurzus frissitesnel " + status );
		return;
		}
	var kurzusok = JSON.parse( data );
	var list_data = [];
	//only cleard unselectded, houldnt add selected again
	for ( i in kurzusok ){
		if ( kurzusChoosen.indexOf( kurzusok[ i ][ "kurzkod" ]) == -1 ) {
			list_data.push(  );
			kurzusList.add([{ "kurzkod" : kurzusok[ i ][ "kurzkod" ], "kurzus" : kurzusok[ i ][ "kurzus" ] }], kurzus_callback );
			}
		}
	}

function toggle_selection_kurzus( ) {
//toggle selection, delet form one list add to other
	var node = $(this);
	var kurzkod = node.attr( "id" );
	toggle_selection_kurzus2( kurzkod, false );
	}

function toggle_selection_kurzus2( kurzkod, last_load ) {
//do it in two steps, for toggler to work
//if last one loaded callback to calendar should clear up loading overlay
	if ( kurzkod === undefined ) {
		return( null );
		}
	var node = $("#" + kurzkod.replace(/:/g,"\\:") );
	var index = kurzusChoosen.indexOf( kurzkod );
	var selected = node.hasClass( "list-group-item-info" );
	if ( selected ) {
		node.removeClass( "list-group-item-info" );
		kurzusChoosen.splice( index, 1 ); 
		remove_from_calendar( kurzkod );
		}
	else {
		node.addClass( "list-group-item-info" );
		kurzusChoosen.push( kurzkod );
		if ( last_load ) {
			$.post( 'test.php', { type : "orak", kurzusChoosen : JSON.stringify( [ kurzkod ] ) },
					function ( data, status ) { push_to_calendar( data, status ); load_overlay_toggle(); } );
		} else {
			$.post( 'test.php', { type : "orak", kurzusChoosen : JSON.stringify( [ kurzkod ] ) }, push_to_calendar );
			}
		}
	}

function clear_kurzusok( ) {
//clear all kurzusok
	kurzusList.clear();
	}

function clear_unselected_kurzusok( ) {
//clear unselected kurzusok
	//we need a copy so we can delet from original
	var items = kurzusList.items.slice(0);
	for ( i = 0; i < items.length; i++ ) {
		var kurzkod = items[ i ].values()[ "kurzkod" ];
		if ( kurzusChoosen.indexOf( kurzkod ) == -1 ) {
			kurzusList.remove( "kurzkod" , kurzkod );
			}
		}
	kurzusList.update();
	}

function save_kurzus_list( ) {
//TODO should have some feedback for saving...
	localStorage.kurzuslist = JSON.stringify( kurzusChoosen );
	}

function load_overlay_toggle( ) {
	$( "#load-overlay" ).toggleClass( "invisible" );
	$( "#kurzusLoadButton" ).toggleClass( "btn-default" );
	$( "#kurzusLoadButton" ).toggleClass( "btn-info" );
}

function load_kurzus_list( ) {
//get kurzus from localstorage, (get data from server, add it to lists)
	var kurzuslist = [];
	//if empty list is save its just [] not loading that
	if ( localStorage.kurzuslist.length > 2 ) {
		load_overlay_toggle();
		kurzuslist = localStorage.kurzuslist;
	}
	else {
		return;
	}
	//this hould clean everything
	clear_kurzusok();
	remove_all_from_calendar()
	console.log( "Loading from local storage: " + kurzuslist );
	$.post( "test.php", { type : "kurzusList", kurzusChoosen : kurzuslist },
		//doing it like this so toggler always comes after everything is loaded
		function( data, status ) { update_kurzusok( data, status ); kurzus_toggler( data ); } );
	}

function kurzus_toggler( data ) {
//toggle all kurzus fresh from loading
	var list = JSON.parse( data );
	for ( i in list ) {
		if ( i == (list.length - 1) ) {
			toggle_selection_kurzus2( list[ i ][ "kurzkod" ], true );
		} else {
			toggle_selection_kurzus2( list[ i ][ "kurzkod" ], false );
			}
		}
	}
