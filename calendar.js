mon = moment().startOf('week');
//table to look up day names
var days = { Hétfo: 0, Kedd : 1, Szerda : 2, Csütörtök : 3, Péntek : 4 }; 
function getDay( day ) {
	ret = moment( mon );
	ret.add( day + 1, "days");
	console.log( day, ret.toString())
	return( ret );
	}

function push_to_calendar( data, status ) {
//get data with ajax, push new stuff to calendar
	var orak = JSON.parse( data );
	var caldata = [];
	var classN = "";
	for (i in orak) {
		//TODO make this part nice
		var day = getDay( days[ orak[ i ][ "nap" ] ] );
		var end = moment.duration( orak[ i ][ "vege" ] );
		var start = moment.duration( orak[ i ][ "kezd" ] );
		var start_d = moment( day );
		start_d.add( start );
		orak[ i ][ "start" ] = start_d;
		var end_d = moment( day );
		end_d.add( end );
		orak[ i ][ "end" ] = end_d;
		var type = "";
		if ( orak[ i ][ "elo" ] > orak[ i ][ "gyak" ] ) {
			type = "elo";
		} else {
			type = "gyak";
		}
		var label = orak[ i ][ "kurzus" ] + " " + type + " (" + orak[ i ][ "csopnum" ] + ")";
		var tanarlist = orak[ i ][ "tanarok" ];
		classN = orak[ i ][ "kurzkod" ] + orak[ i ][ "csopnum" ];
		caldata.push( {
					title : label,
					start : orak[ i ][ "start" ],
					end : orak[ i ][ "end" ],
					className : [ classN, orak[ i ][ "kurzkod" ], "magic" ],
					id : classN,
					csopnum : orak[ i ][ "csopnum" ],
					kurzkod : orak[ i ][ "kurzkod" ],
					kurzus : orak[ i ][ "kurzus" ],
					tanarok : tanarlist,
					elo : orak[ i ][ "elo" ],
					gyak : orak[ i ][ "gyak" ],
					hely : orak[ i ][ "epulet" ] + " " + orak[ i ][ "terem" ],
				} );
		}
	cal.fullCalendar( 'addEventSource', { events : caldata, color : getRandomColor(), textColor : "#FFF" } );
	console.log( JSON.stringify( caldata ) );
	cal.fullCalendar( 'render' );
	}

function remove_from_calendar( kurzkod ) {
	//remove everythinh macthing kurzkod
	cal.fullCalendar( 'removeEvents', function( ev ){
		return( (ev.kurzkod == kurzkod) )
	})
}

function remove_all_from_calendar( ) {
	//remove everything
	cal.fullCalendar( 'removeEvents' )
}

function event_click_callback( ev, jsev, view ) {
	//toggle colorful state
	if (ev.color == "#FFF" ) {
		ev.color = ev.source.color;
		ev.textColor = ev.source.textColor;
	} else {
		ev.color = "#FFF";
		ev.textColor = "#000";
	}
	$( jsev.currentTarget ).popover( "hide" );
	cal.fullCalendar( 'updateEvent', ev );
}

function event_color_toggle( ev, jsev, view ) {
	//toggle colorful state
	$( "." + ev.id ).toggleClass( "blur" );
	$( jsev.currentTarget ).popover( "toggle" );
	$( "." + ev.kurzkod ).toggleClass( "blur2" );
}

function add_popover( ev, el, view ) {
	//for each occurence of each event, make a popover
	//TODO nicer popovers
	$( el ).each( function( i ) {
		var text = "<p class='prop-header'>Kurzus:</p><p class='prop-content'>" + ev.kurzus + "</p>" +
			"<p class='prop-header'>Csoport:</p><p class='prop-content'>" + ev.csopnum + "</p>";
		text += "<p class='prop-header'>Tanarok:</p><p class='prop-content'>"
		for ( i in ev.tanarok ) {
			text += ev.tanarok[i].nev;
			if ( ev.tanarok[i].email != "" ) {
				text += " (" + ev.tanarok[i].email + ")";
			}
			text += ", ";
		}
		text += "</p><p class='prop-header'>Hely:</p><p class='prop-content'>" + ev.hely  + "</p>";
		if (ev.elo != 0) {
			text += "<p class='prop-content elogyak'>Eloadas</p>";
		} else {
			text += "<p class='prop-content elogyak'>Gyak/Labor</p>";
		}
		//since fc-time-grid-container is inline overfowl:hidden, popovers should be outside that
		//so: container body
		var place = "bottom";
		if ( ev.start.hour() > 12 ) {
			place = "top";
		}
		$( this ).popover( {
			title : ev.kurzkod,
			content : text,
			trigger : "manual",
			placement : place,
			html : true,
			container : 'body',
		} );
		console.log( ev.start.hour() );
		$( this ).css( "zIndex", -1 );
	} );
}

function getRandomColor() {
	var letters = '0369'.split('');
	var color = '#';
	for (var i = 0; i < 6; i++ ) {
		color += letters[Math.floor(Math.random() * 4)];
		}
	return( color );
}
