/*
Copyright 2015 Kocsis Matyas <matyilona@openmaibox.org>

This file is part of MagicalOrarend.

MagicalOrarend is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

MagicalOrarend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MagicalOrarend. If not, see <http://www.gnu.org/licenses/>.
*/

$(document).ready( function() {
	$(".szakContainer").on( "click", ".szak_list", toggle_selection_szak );
	$(".kurzusContainer").on( "click", ".kurzus_list", toggle_selection_kurzus );
	$.post( 'test.php', { type : "szakok" }, update_szakok );
	$("#kurzusSaveButton").click( save_kurzus_list );
	$("#kurzusLoadButton").click( load_kurzus_list );
	cal = $("#timetable")
	cal.fullCalendar({
		schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
		height: 630,
		weekends: false,
		defaultView: 'agendaWeek',
		allDaySlot: false,
		minTime: "08:00:00",
		maxTime: "22:00:00",
		slotLabelFormat: "HH:mm",
		header: false,
		columnFormat: 'ddd',
		slotEventOverlap: false,
		eventClick: event_click_callback,
		eventMouseover: event_color_toggle,
		eventMouseout: event_color_toggle,
		eventAfterRender: add_popover,
		});
	szakList = new List('szaklist', { valueNames: ["szak", "szakazon"],
       		item: '<li class="szak_list list-group-item"><div class="szakcont"><div class="szak"></div><small class="small szakazon"></small></div></li>'}, [] );
	kurzusList = new List('kurzuslist', { valueNames: ["kurzus", "kurzazon"],
		item: '<li class="kurzus_list list-group-item"><div class="kurzuscont"><div class="kurzus"></div><small class="small kurzkod"></small></div></li>'}, [] );
	});
